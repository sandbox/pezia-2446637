<?php

/**
 * @file
 * Tests Wrap Every N Rows.
 */

/**
 * Implements hook_views_default_views().
 */
function wrap_every_n_rows_test_views_default_views() {
  $view = new view();
  $view->name = WrapEveryNRowsTestCase::TEST_VIEW_NAME;
  $view->description = 'Test View for Wrap Every n Rows';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = '3.0';
  $view->core = 7;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $master = $view->new_display('default', 'Master', 'default');
  $master->display->display_options['title'] = 'Wrap Every N Rows Test';
  $master->display->display_options['access']['type'] = 'none';
  $master->display->display_options['cache']['type'] = 'none';
  $master->display->display_options['exposed_form']['type'] = 'basic';
  $master->display->display_options['pager']['type'] = 'some';
  $master->display->display_options['pager']['options']['items_per_page'] = '10';
  $master->display->display_options['pager']['options']['offset'] = '0';
  $master->display->display_options['style_plugin'] = 'wrap_every_n';
  $master->display->display_options['row_plugin'] = 'node';
  $master->display->display_options['row_options']['links'] = FALSE;
  $master->display->display_options['style_options']['wrapper_opening'] = '<div class="myfancytestrow">';
  $master->display->display_options['style_options']['wrapper_closing'] = '</div>';
  $master->display->display_options['style_options']['wrappable_element_count'] = '3';

  /* Display: Page */
  $page = $view->new_display('page', 'Page', 'test_wrap_every_n_rows_page');
  $page->display->display_options['path'] = 'test_wrap_every_n_rows';

  $views[$view->name] = $view;

  return $views;
}
