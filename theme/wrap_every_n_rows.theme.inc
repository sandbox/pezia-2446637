<?php

function _wrap_every_n_rows_preprocess_wrap_every_n_rows(&$vars) {
  $options = $vars['options'];

  $vars['wrapper_opening'] = $options['wrapper_opening'];
  $vars['wrapper_closing'] = $options['wrapper_closing'];
  $vars['wrappable_element_count'] = $options['wrappable_element_count'];
}
