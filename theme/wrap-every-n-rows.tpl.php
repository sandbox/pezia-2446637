<?php
$rowCounter = 0; // maybe the $id in the foreach is enough
end($rows);
$lastId = key($rows);
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <?php
  if ($rowCounter % $wrappable_element_count === 0) {
    echo $wrapper_opening;
  }
  ?>
  <?php print $row; ?>
  <?php
  if ($rowCounter % $wrappable_element_count === $wrappable_element_count - 1 || $id === $lastId) {
    echo $wrapper_closing;
  }
  ?>
  <?php $rowCounter++; ?>
<?php endforeach; ?>
