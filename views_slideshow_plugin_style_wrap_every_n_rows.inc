<?php

/**
 * @file
 * Contains the wrap every N-th style plugin.
 */

/**
 * Style plugin to render each item in a slideshow of an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 */
class views_slideshow_plugin_style_wrap_every_n_rows extends views_plugin_style {

  /**
   * Set the default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['wrapper_opening'] = '<div class="row">';
    $options['wrapper_closing'] = '</div>';
    $options['wrappable_element_count'] = 0;

    return $options;
  }

  /**
   * Build the options form
   *
   * @param array $form
   * @param array $form_state
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['wrapper_opening'] = array(
      '#type' => 'textfield',
      '#title' => t('Wrapper opening'),
      '#default_value' => $this->options['wrapper_opening'],
    );

    $form['wrapper_closing'] = array(
      '#type' => 'textfield',
      '#title' => t('Wrapper closing'),
      '#default_value' => $this->options['wrapper_closing'],
    );

    $form['wrappable_element_count'] = array(
      '#type' => 'textfield',
      '#title' => t('Wrap every this number of elements'),
      '#default_value' => $this->options['wrappable_element_count'],
    );
  }

  /**
   * Validate options
   * 
   * @param array $form
   * @param array $form_state
   */
  function options_validate(&$form, &$form_state) {
    element_validate_number($form['wrappable_element_count'], $form_state);
  }

}
