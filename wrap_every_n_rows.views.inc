<?php

/**
 * @file
 * Defines the View Style Plugins for Views Slideshow module.
 */

/**
 * Implements hook_views_plugins().
 */
function wrap_every_n_rows_views_plugins() {
  return array(
    'style' => array(
      'wrap_every_n' => array(
        'title' => t('Wrap every N rows'),
        'help' => t('Display the results as an unformatted list with every N items wrapped.'),
        'handler' => 'views_slideshow_plugin_style_wrap_every_n_rows',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses grouping' => FALSE,
        'uses row class' => FALSE,
        'type' => 'normal',
        'path' => drupal_get_path('module', 'wrap_every_n_rows'),
        'theme' => 'wrap_every_n_rows',
        'theme path' => drupal_get_path('module', 'wrap_every_n_rows') . '/theme',
        'theme file' => 'wrap_every_n_rows.theme.inc',
      ),
    ),
  );
}
